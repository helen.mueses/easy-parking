from Conexion_db import Conexion_db

class Crud:
    def __init__(self,host,db,user,passwd):
        self.my_conn = Conexion_db(host,db,user,passwd)
        #creando un objeto de la clase conexion_db y definiendolo
        #como atributo de la clase Crud (self)
    
    #escribir metodos para escribir, leer, eliminar,actualizar registros de tablas
    #segun las necesidades de mi proyecto

    def insertar_vehiculo(self, placa, tipo):
        query = "INSERT INTO \"Vehiculos\""+\
            "(placa, tipo)"+\
                "VALUES ('"+placa+ "','"+tipo+"')"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)

##*******************************************

    def insertar_factura(self, vehiculo_id, operario_id, entrada, salida, tiempoFacturado, tarifa_id, subtotal, iva, modoPago, totalPagado):
        query = "INSERT INTO \"Facturas\""+\
            "(vehiculo_id, operario_id, entrada, salida, tiempoFacturado, tarifa_id, subtotal, iva, modoPago, totalPagado)"+\
                "VALUES ('"+vehiculo_id+ "','"+operario_id+"','"+entrada+"','"+salida+"','"+tiempoFacturado+"','"+tarifa_id+"','"+subtotal+"','"+iva+"','"+modoPago+"','"+totalPagado+"')"
        print(query)#for test purposes
        self.my_conn.escribir_db(query)


##*******************************************

    def leer_facturas(self):
        query = "SELECT id,vehiculo_id, operario_id, entrada, salida, tiempoFacturado, tarifa_id, subtotal, iva, modoPago, totalPagado"+\
            " FROM \"Facturas\";"
        print(query)
        respuesta=self.my_conn.consultar_db(query)
        return respuesta


    def cerrar(self):
        self.my_conn.cerrar_db()