"""
from flask import Flask,request,make_response,redirect
from flask.wrappers import Response #clase Flask de extension flask
app = Flask(__name__) #instancia de Flask
from Crud import Crud
import json #para poder dar formato json 
from datetime import date, datetime



#@app.route('/')#ruta en el navegador
#def hello():#funcion que se realiza cuando se acceda a esa ruta
#    user_ip = request.remote_addr # solicitando direccion ip al navegador (cliente)
#    print(user_ip)

#    response = make_response(redirect('/get_bus'))#para direccionar hacia la ruta /get_bus
#    response.set_cookie('user_ip',user_ip)#estableciendo cookie en el navegador    
#    return response
    #retorno = json.dumps({"direccion ip":user_ip})
    #return retorno
    #numero=5
    #texto = "numero: "+str(numero)
    #return texto #retorna el string 
 

@app.route('/')
def hello():
    response = make_response(redirect('/get_factura'))#para direccionar hacia la ruta /get_factura    
    return response

#@app.route("/get_bus")
#def leer_bus():

#    crud = Crud("localhost","transportes_S3", "postgres","admin")
#    buses = crud.leer_buses()
#    primer_registro = buses[0]
#    #convertir a string
#    print(primer_registro)
#    la_ip = request.cookies.get('user_ip')


@app.route("/login", methods=["GET","POST"])
def login():
    nombre_usuario="admin"
    password="admin"
    if(request.method=="GET"):
        return render_template("login.html")
    if(request.method=="POST"):
        username=request.form.get("username")
        passwd = request.form.get("passwd")
        if(username==nombre_usuario and passwd==password):
            response = make_response(redirect("/get_factura"))
            return response
        else:
            return render_template("login.html",acceso="incorrecto")



@app.route("/get_factura")
def leer_factura():

    crud = Crud("ec2-54-146-84-101.compute-1.amazonaws.com","dc9joadcnmu80s","zgewgxwysxmgno","5a4b1c3f4bce9f270cec01646a9777ed6ed4590c4082becbfa01c4a95ab75f53")
    facturas = crud.leer_facturas()
    primer_registro = facturas[0]
    print("Primer registro: ", primer_registro)

    #registro = "id="+str(primer_registro[0])+" modelo="+str(primer_registro[1])+\
    #    " capacidad="+str(primer_registro[2])+" placa="+primer_registro[3]+\
    #        " marca="+primer_registro[4]
    #retornar string
    retorno = json.dumps({"id":primer_registro[0],"modelo":primer_registro[1],
     "capacidad":primer_registro[2],"placa":primer_registro[3],"marca":primer_registro[4]})
    return retorno

@app.route("/get_buses") 
def leer_buses():
    crud = Crud("localhost","transportes_S3", "postgres","admin")
    buses = crud.leer_buses()
    respuesta = []
    for registro in buses: 
        respuesta.append({"id":registro[0], "modelo":registro[1],
     "capacidad":registro[2],"placa":registro[3],"marca":registro[4]})

    respuesta = json.dumps(respuesta)
    
    respuesta = ""
    for registro in buses: #registro toma el valor de cada posicion de la lista buses (registro es la tupla)
        respuesta = respuesta + "<b>id=</b>"+str(registro[0])+" modelo="+str(registro[1])+\
        " capacidad="+str(registro[2])+" placa="+registro[3]+\
            " marca="+registro[4] +  "</br>"
    
    return respuesta 


@app.route("/get_viaje") 
def leer_viaje():
    crud = Crud("localhost","transportes_S3", "postgres","admin")
    viajes = crud.leer_viajes()
    primer_registro = viajes[0] #primera tupla
    print("el primer registro=", primer_registro)
    hora_inicio = primer_registro[3]#posicion tres de la tupla
    hora_fin = primer_registro[4] #posicion 4 de la tupla
    hora_inicio_string = hora_inicio.strftime("%d/%m/%Y - %H:%M:%S") #convirtiendo datetime a string
    hora_fin_string = hora_fin.strftime("%d/%m/%Y - %H:%M:%S")    
    print(hora_inicio)
    print(type(hora_inicio))
    print(hora_fin)
    print(type(hora_fin)) 
    print("la diferencia en minutos=", (hora_fin - hora_inicio).total_seconds() / 60)
    respuesta = json.dumps({"id":primer_registro[0] ,"id_pasajero":primer_registro[1],
     "id_trayecto":primer_registro[2],
    "hora inicio":hora_inicio_string,"hora fin":hora_fin_string})
    return respuesta

@app.route("/get_viajes") 
def leer_viajes():
    crud = Crud("localhost","transportes_S3", "postgres","admin")
    viajes = crud.leer_viajes()
    respuesta = []
    for registro in viajes:

        hora_inicio = registro[3]#posicion tres de la tupla
        hora_fin = registro[4] #posicion 4 de la tupla
        hora_inicio_string = hora_inicio.strftime("%d/%m/%Y - %H:%M:%S") #convirtiendo datetime a string
        hora_fin_string = hora_fin.strftime("%d/%m/%Y - %H:%M:%S")    
        respuesta.append({"id":registro[0] ,"id_pasajero":registro[1],
        "id_trayecto":registro[2],
        "hora inicio":hora_inicio_string,"hora fin":hora_fin_string})
    
    respuesta = json.dumps(respuesta)
    
    return respuesta



if __name__=="__main__": #punto de partida, donde python empieza a ejecutar
	while(True):
		print("starting web server")
		app.run(debug=True,host='0.0.0.0')#arranca el servidor, 0.0.0.0->localhost
        #en el navegador se accede localhost:5000/

"""