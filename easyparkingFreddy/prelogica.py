from Crud import Crud
from datetime import datetime

my_crud = Crud("ec2-54-146-84-101.compute-1.amazonaws.com","dc9joadcnmu80s","zgewgxwysxmgno","5a4b1c3f4bce9f270cec01646a9777ed6ed4590c4082becbfa01c4a95ab75f53")




tarifa_auto = 70 #int (input("digite la tarifa minuto/AUTOMOVIL"))
tarifa_moto = 40 #int (input("digite la tarifa minuto/MOTO"))
tarifa_bici = 20 #int (input("digite la tarifa minuto/BICICLETA"))

plazas_auto = 6 #int (input("digite el total de plazas para AUTOMOVIL"))
plazas_moto = 6 #int (input("digite el total de plazas para MOTO"))
plazas_bici = 6 #int (input("digite el total de plazas para BICICLETA"))

alarma_auto = 2 #int (input("digite el total de plazas libres para alarma AUTOMOVIL"))
alarma_moto = 2 #int (input("digite el total de plazas libres para alarma MOTO"))
alarma_bici = 2 #int (input("digite el total de plazas libres para alarma BICICLETA"))


list_autos={}
cont_autos=0
list_motos={}
cont_motos=0
list_bici={}
cont_bici=0
iva=0
operario_id =  1
modoPago = 0


op=-1
    
while op != 0:
        op = int(input("""\n--Selecciona una opción de 1 a 3 ó CERO para salir--
1. Automóvil
2. Moto
3. Bicicleta
0. Salir

Ingresa un número válido de opción del menú: """))
    

##***************************************************************

        if (op == 1):
            
            placa = input("Digite la placa o cero para salir: ")
            if (placa in list_autos):
                #hs = datetime.datetime.today()
                hs = datetime.now()
                print ("Hora salida: ", hs)
                hs_string = hs.strftime("%d/%m/%Y - %H:%M:%S")
                #hs_string = hs.strtime("%Y/%m/%d-%H:%M:%S")
                #hs_string = datetime.strftime(hs)
                #print (hs_string)
                he = (list_autos[placa])
                
                print ("Hora entrada: ", he)
                he_string = he.strftime("%d/%m/%Y - %H:%M:%S")
                #he_string = he.strtime("%Y/%m/%d-%H:%M:%S")

                deltatiempo = hs - he
                print (deltatiempo)

                tiempo = (deltatiempo.total_seconds())/60

                minutos = int(tiempo)
                print ('El tiempo es ', minutos, 'mins')

                cobro = tarifa_auto * minutos
                print ('Cobro $',cobro)

                con_iva = cobro + iva

                list_autos.pop(placa)

                cont_autos-=1
                plazas_disponibles_auto = plazas_auto - cont_autos
                print ("Plazas disponibles para AUTOMOVIL: ", plazas_disponibles_auto)
    
#######
######EN ESTE PUNTO SE ALMACENAN LOS DATON EN BD
#######

                my_crud.insertar_vehiculo(placa,"automovil")
                #my_crud.insertar_factura(placa, operario_id, he_string, hs_string, minutos, tarifa_auto, cobro, iva, modoPago, con_iva)
                my_crud.cerrar()


        
            elif (placa!="0" and (cont_autos < plazas_auto)):
                #he = datetime.datetime.today()
                he = datetime.now()
                list_autos[placa]=he

                cont_autos+=1
                plazas_disponibles_auto = plazas_auto - cont_autos
                if (plazas_disponibles_auto == alarma_auto):
                    print ("---ALARMA---: QUEDAN SOLO ", plazas_disponibles_auto, "PLAZAS DISPONIBLES PARA AUTOMOVIL")
                elif (cont_autos == plazas_auto):
                    print ("---ALARMA---: ULTIMA PLAZA DISPONIBLE, Cupo para AUTOMOVILES completo")
                else:
                    print ("Plazas disponibles para AUTOMOVIL: ", plazas_disponibles_auto)
    
            else:
                print ("************* NO HAY CUPO ****************")

            print(list_autos)


##***************************************************************
       
        elif (op == 2):
            
            placa = input("Digite la placa o cero para salir: ")
            if (placa in list_motos):
                hs = datetime.datetime.today()
                print ("Hora salida: ", hs)
                he = (list_motos[placa])
                print ("Hora entrada: ", he)

                deltatiempo = hs - he
                print (deltatiempo)

                tiempo = (deltatiempo.total_seconds())/60

                minutos = int(tiempo)
                print ('El tiempo es ', minutos, 'mins')

                cobro = tarifa_moto * minutos
                print ('$',cobro)

                list_motos.pop(placa)

                cont_motos-=1
                plazas_disponibles_moto = plazas_moto - cont_motos
                print ("Plazas disponibles para AUTOMOVIL: ", plazas_disponibles_moto)
    
#######
######EN ESTE PUNTO SE ALMACENAN LOS DATON EN BD
#######


                my_crud.insertar_vehiculo(placa,"Moto")
                my_crud.cerrar()

        
            elif (placa!="0" and (cont_motos < plazas_moto)):
                he = datetime.datetime.today()
                list_motos[placa]=he

                cont_motos+=1
                plazas_disponibles_moto = plazas_moto - cont_motos
                if (plazas_disponibles_moto == alarma_moto):
                    print ("---ALARMA---: QUEDAN SOLO ", plazas_disponibles_moto, "PLAZAS DISPONIBLES PARA MOTO")
                elif (cont_motos == plazas_moto):
                    print ("---ALARMA---: ULTIMA PLAZA DISPONIBLE, Cupo MOTOS completo")
                else:
                    print ("Plazas disponibles para MOTO: ", plazas_disponibles_moto)

            else:
                print ("************* NO HAY CUPO ****************")
    
            print(list_motos)    

##***************************************************************
            
        elif (op == 3):
            
            documento = input("Digite el Documento o cero para salir: ")
            if (documento in list_bici):
                hs = datetime.datetime.today()
                print ("Hora salida: ", hs)
                he = (list_bici[documento])
                print ("Hora entrada: ", he)

                deltatiempo = hs - he
                print (deltatiempo)

                tiempo = (deltatiempo.total_seconds())/60

                minutos = int(tiempo)
                print ('El tiempo es ', minutos, 'mins')

                cobro = tarifa_bici * minutos
                print ('$',cobro)

                list_bici.pop(documento)

                cont_bici-=1
                plazas_disponibles_bici = plazas_bici - cont_bici
                print ("Plazas disponibles para BICICLETA: ", plazas_disponibles_bici)
    
#######
######EN ESTE PUNTO SE ALMACENAN LOS DATON EN BD
#######

                my_crud.insertar_vehiculo(documento,"Bicicleta")
                my_crud.cerrar()

        
            elif (documento!="0" and (cont_bici < plazas_bici)):
                he = datetime.datetime.today()
                list_bici[documento]=he

                cont_bici+=1
                plazas_disponibles_bici = plazas_bici - cont_bici
                if (plazas_disponibles_bici == alarma_bici):
                    print ("---ALARMA---: QUEDAN SOLO ", plazas_disponibles_bici, "PLAZAS DISPONIBLES PARA BICICLETA")
                elif (cont_bici == plazas_bici):
                    print ("---ALARMA---: ULTIMA PLAZA DISPONIBLE, Cupo BICICLETAS completo")
                else:
                    print ("Plazas disponibles para BICICLETA: ", plazas_disponibles_bici)
    
            else:
                print ("************* NO HAY CUPO ****************")

            print(list_bici)